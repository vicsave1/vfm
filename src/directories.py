import os
import sys
import pathlib

from colorama import Fore as cl

HOME = os.environ['HOME']


def GetWorkingDir(): return os.getcwd()  # Returns cwd (like /home/vicsave)
def OpenDirectory(path): os.chdir(path)  # Changes cwd


def CreateDirectory(directory_name='New Folder'):
    if os.path.exists(f'{GetWorkingDir()}/{directory_name}'): return
    os.mkdir(GetWorkingDir() + '/' + directory_name)

def CreateDirectoryAt(path, directory_name='New Folder'):
    if os.path.exists(f'{path}/{directory_name}'): return
    os.mkdir(f'{path}/{directory_name}')

def RemoveDirectory(directory):
    if not os.path.exists(f'{GetWorkingDir()}/{directory}'): return
    os.rmdir(f'{GetWorkingDir()}/{directory}')


def ListDirectory(*args, directory=""):
    if directory == "":
        directory = GetWorkingDir()
    
    paths = []
    
    show_hidden = False
    colors = False

    print(GetWorkingDir() + ":")

    if 'show-hidden' in args:
        show_hidden = True
    if 'colors' in args:
        colors = True

    for path in pathlib.Path(directory).iterdir():
        if show_hidden:
            if colors:
                if os.path.isdir(path):
                    paths.append(f'\033[1m{cl.CYAN}{str(path).replace(directory+"/", "")}{cl.RESET}\033[0m')
                else:
                    paths.append(str(path).replace(directory+'/', ''))
            else:
                paths.append(str(path).replace(directory+'/', ''))
        
        else:
            if str(path).replace(directory+"/", "").startswith('.'): continue
            if colors:
                if os.path.isdir(path):
                    paths.append(f'\033[1m{cl.CYAN}{str(path).replace(directory+"/", "")}{cl.RESET}\033[0m')
                else:
                    paths.append(str(path).replace(directory+'/', ''))
            else:
                paths.append(str(path).replace(directory+'/', ''))

            
    sorted_paths = sorted(paths)

    for i in sorted_paths:
        sys.stdout.write(f'{i} ')
        sys.stdout.flush()
    sys.stdout.write('\n')
